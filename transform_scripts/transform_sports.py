import pandas as pd

# Read the original CSV file
df = pd.read_csv('sports_sitemap.csv')

# Select only the desired columns
selected_columns = ['list', 'nation','total_medals','gold_medals','silver_medals','bronze_medals']
df_selected = df[selected_columns]

# Save the selected columns to a new JSON file
df_selected.to_json('sports.json', orient='records')

# The rest of the columns (if any) will remain in the original DataFrame but not included in df_selected
