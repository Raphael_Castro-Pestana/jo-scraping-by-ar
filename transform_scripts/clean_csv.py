import os
import json
from collections import defaultdict

def convert_medal_type(medal_type):
    if medal_type == 1:
        return "gold_medals"
    elif medal_type == 2:
        return "silver_medals"
    elif medal_type == 3:
        return "bronze_medals"

def aggregate_data(folder_path):
    athlete_medals = defaultdict(lambda: defaultdict(int))

    # Iterate through each JSON file in the folder
    for filename in os.listdir(folder_path):
        if filename.endswith('.json'):
            file_path = os.path.join(folder_path, filename)
            with open(file_path, 'r') as file:
                data = json.load(file)
                for obj in data:
                    athlete_name = obj['element']
                    medal_type = obj['medal_type']
                    medal_key = convert_medal_type(medal_type)
                    athlete_medals[athlete_name][medal_key] += 1
                    athlete_medals[athlete_name]['team'] = obj['team']
                    athlete_medals[athlete_name]['sport'] = obj['sport']

    return athlete_medals

def main():
    folder_path = 'json'  # Specify the folder path containing JSON files
    final_data = []

    # Aggregate data from all JSON files in the folder
    athlete_medals = aggregate_data(folder_path)

    # Create final JSON objects
    for athlete, medals in athlete_medals.items():
        final_data.append({
            "element": athlete,
            "team": medals['team'],
            "sport": medals['sport'],
            "gold_medals": medals['gold_medals'],
            "silver_medals": medals['silver_medals'],
            "bronze_medals": medals['bronze_medals']
        })

    # Save the aggregated data into a single output file
    with open('athletes_data.json', 'w') as file:
        json.dump(final_data, file, indent=4)

if __name__ == "__main__":
    main()