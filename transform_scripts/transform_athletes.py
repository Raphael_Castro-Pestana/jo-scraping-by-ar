import pandas as pd
import re

# Read the original CSV file
df = pd.read_csv('athletes_q.csv')

# Select only the desired columns
selected_columns = ['element', 'medal_type', 'team', 'sport']
df_selected = df[selected_columns].copy()  # Explicitly create a copy of the DataFrame

# Remove unwanted substring from the data
df_selected.replace({r'\n\n\t\n\n\n\t\t\t\t\t\n\t\t\t\t\t\t': ' '}, regex=True, inplace=True)

# Save the selected columns to a new JSON file with UTF-8 encoding
with open('athletes_q.json', 'w', encoding='utf-8') as f:
    df_selected.to_json(f, orient='records', force_ascii=False)